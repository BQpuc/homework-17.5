#pragma once
#include <iostream>
using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "Animal voice\n";
	}
};
class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Woof\n";
	}
};
class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow\n";
	}
};
class Horse : public Animal
{
public:
	void Voice() override
	{
		cout << "Igogo\n";
	}
};


