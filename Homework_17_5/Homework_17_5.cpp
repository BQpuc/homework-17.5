﻿#include <iostream>
#include <string>
#include "MyHomework.h"
using namespace std;

struct Student
{
	int Age{ 0 }, Height = 0;
	string Name = "Name";
	void GetInfo()
	{
		cout << Age << "\n" << Height;
		cout << "\n" << Name << "\n";
	}
};

int main()
{
	Student a{ 28, 174, "Boris" };
	Student* ptr = new Student
	{33, 164, "Sveta"};
	a.GetInfo();
	ptr->GetInfo();
	delete ptr;
}

